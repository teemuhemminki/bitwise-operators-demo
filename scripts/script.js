//---References---

var inputA = document.getElementsByName("input")[0];
var inputB = document.getElementsByName("input")[1];
var output = document.getElementById("output");



//--Variables--

var descriptions = [
"And operation compares each bit on both values. <br>If both bits are 1, then result will have 1 on same place.",
"Or operation compares each bit on both values. <br>If either or both bits are 1, then result will have 1 on same place.",
"Xor operation compares each bit on both values. <br>If only either one of the bits is 1, then result will have 1 on same place.",
"Left bitshift operation moves bits of 1st value to left by integer amount of 2nd value. <br>This appears as added zeroes on right side",
"Right bitshift operation moves bits of 1st value to right by integer amount of 2nd value. <br>Note that bits moved out of bounds are lost."
]



//---Functions---

//Calculates the results and gives them for writing function
var calculate = function(){
	var a = parseInt(inputA.value);
	var b = parseInt(inputB.value);
	
	var results = [];
	
	results.push(generateOutput(a, b, "And", descriptions[0], a & b)); //Note that the calculation itself happens as the last parameter
	results.push(generateOutput(a, b, "Or", descriptions[1], a | b));
	results.push(generateOutput(a, b, "Xor", descriptions[2], a ^ b));
	results.push(generateOutput(a, b, "Left bitshift", descriptions[3], a << b));
	results.push(generateOutput(a, b, "Right bitshift", descriptions[4], a >> b));	
		
	write(results);
}

//Clears the output div and adds new output
var write = function(results){
	output.innerHTML = "";

	for(var i = 0; i < results.length; i++){
		output.append(results[i]);
	}
}

//Creates a string with binary values of the given number
var toBinary = function(number){
	return (number).toString(2);
}

//Generates the output of given operation
var generateOutput = function(a, b, operator, description, result){

	var binaries = [];
	
	binaries.push(toBinary(a));
	binaries.push(toBinary(b));
	binaries.push(toBinary(result));

	setBinaryLength(binaries);
	
	var div = document.createElement("div");	
	
	var header = document.createElement("h2");
	header.innerHTML = operator;
	div.append(header);
	
	var desc = document.createElement("p");
	desc.innerHTML = description;
	div.append(desc);
		
	var table = document.createElement("table");
	
	var row1 = table.insertRow(0);
	var r1cell1 = row1.insertCell(0);
	var r1cell2 = row1.insertCell(1);
	var r1cell3 = row1.insertCell(2);
	
	var row2 = table.insertRow(0);
	var r2cell1 = row2.insertCell(0);
	var r2cell2 = row2.insertCell(1);
	var r2cell3 = row2.insertCell(2);
	
	var row3 = table.insertRow(0);
	var r3cell1 = row3.insertCell(0);
	var r3cell2 = row3.insertCell(1);
	var r3cell3 = row3.insertCell(2);

	var row4 = table.insertRow(0);
	var r4cell1 = row4.insertCell(0);
	var r4cell2 = row4.insertCell(1);
	var r4cell3 = row4.insertCell(2);
	
	r1cell1.innerHTML = "Result";
	r1cell2.innerHTML = result;
	r1cell3.innerHTML = binaries[2];
	
	r2cell1.innerHTML = "2nd value";
	r2cell2.innerHTML = b;
	r2cell3.innerHTML = binaries[1];
	
	r3cell1.innerHTML = "1st value";
	r3cell2.innerHTML = a;
	r3cell3.innerHTML = binaries[0];
	
	r4cell1.innerHTML = "";
	r4cell2.innerHTML = "Integer";
	r4cell3.innerHTML = "Binary";
	
	div.append(table);
	
	return div;
}

//Equalizes the output length of each binary value in a result
//For example, results 1, 11 and 1000 will be represented as 0001, 0011 and 1000
var setBinaryLength = function(binaries){	
	var longest = 0;
	
	for(var i = 0; i < binaries.length; i++){
		if(binaries[i].length > longest){
			longest = binaries[i].length;
		}
	}
	
	for(var i = 0; i < binaries.length; i++){
		if(binaries[i].length < longest){
			while(binaries[i].length < longest){
				binaries[i] = "0" + binaries[i];
			}
		}
	}
	
	return binaries;
}











